//
//  HeightViewController.swift
//  Celecare
//
//  Created by Dominic Smith on 12/15/17.
//  Copyright © 2017 Celecare LLC. All rights reserved.
//

import UIKit
import Parse
import SCLAlertView
import NVActivityIndicatorView
import SnapKit

class HeightViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "HelveticaNeue-Bold", size: 30)
        label.text = "What's your Height?"
        label.textColor = UIColor.black
        label.numberOfLines = 0
        return label
    }()
    
    lazy var pickerView: UIPickerView = {
        let pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.backgroundColor = .clear
        
        return pickerView
    }()
    
    lazy var signupButton: UIButton = {
        let button = UIButton()
        button.setTitle("Done", for: .normal)
        button.titleLabel?.font = UIFont(name: "HelveticaNeue", size: 20)
        button.setTitleColor(.white, for: .normal)
        button.setBackgroundColor(uicolorFromHex(0x006a52), forState: .normal)
        button.titleLabel?.textAlignment = .center
        button.addTarget(self, action: #selector(self.buttonAction(sender:)), for: .touchUpInside)
        return button
    }()
    
    var heightFt = [ "0", "1'", "2'", "3'", "4'", "5'", "6'", "7'", "8'" ]
    var heightIn = [ "0\"", "1\"", "2\"", "3\"", "4\"", "5\"", "6\"", "7\"", "8\"", "9\"", "10\"", "11\""]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(titleLabel)
        self.view.addSubview(pickerView)
        self.view.addSubview(signupButton)
        
        titleLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.view).offset(75)
            make.left.equalTo(self.view).offset(10)
            make.right.equalTo(self.view).offset(-10)
        }
        
        pickerView.snp.makeConstraints { (make) -> Void in
            // make.top.equalTo(titleLabel.snp.bottom).offset(75)
            make.bottom.equalTo(self.signupButton.snp.top)
            make.left.equalTo(self.view)
            make.right.equalTo(self.view)
        }
        
        signupButton.snp.makeConstraints { (make) -> Void in
            make.height.equalTo(50)
            // make.top.equalTo(datePicker.snp.bottom)
            make.bottom.equalTo(self.view)
            make.left.equalTo(self.view)
            make.right.equalTo(self.view)
        }
        
        // Do any additional setup after loading the view.
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0{
            return heightFt.count
            
        } else {
            return heightIn.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0{
            return heightFt[row]
            
        } else {
            return heightIn[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
    }
    
    @objc func buttonAction(sender: UIButton){
        
    }
    
    func uicolorFromHex(_ rgbValue:UInt32)->UIColor{
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        
        return UIColor(red:red, green:green, blue:blue, alpha:1.0)
    }

}
