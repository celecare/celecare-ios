//
//  AnswerViewController.swift
//  Sickcall
//
//  Created by Mac Owner on 7/10/17.
//  Copyright © 2017 Sickcall LLC All rights reserved.
//

import UIKit
import Parse
import Kingfisher
import NVActivityIndicatorView

class AnswerViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, NVActivityIndicatorViewable {
    
    var objectId = [String]()
    var isAnswered = [Bool]()
    var chargeId = [String]()

    var painMoveStay = [String]()
    var painBetterWorse = [String]()
    var currentMedications = [String]()
    var howLongResponse = [String]()
    var releventInfoResponse = [String]()
    var symptomCauses = [String]()
    var painResponse = [String]()
    var healthImage = [String]()
    
    var newQuestion = false
    
    var advisorUserId = [String]()
    var advisorLevel = [String]()
    var advisorResponse = [String]()
        
    let screenSize: CGRect = UIScreen.main.bounds
    
    @IBOutlet weak var tableJaunt: UITableView!
    
    var refreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.title = "My Questions"
        
        startAnimating()

        self.tableJaunt.register(MainTableViewCell.self, forCellReuseIdentifier: "myQuestionsReuse")
        self.tableJaunt.register(MainTableViewCell.self, forCellReuseIdentifier: "noQuestionsReuse")
        self.tableJaunt.estimatedRowHeight = 50
        self.tableJaunt.rowHeight = UITableViewAutomaticDimension
        
        NVActivityIndicatorView.DEFAULT_TYPE = .ballSpinFadeLoader
        NVActivityIndicatorView.DEFAULT_COLOR = .white
        NVActivityIndicatorView.DEFAULT_BLOCKER_SIZE = CGSize(width: 60, height: 60)
        NVActivityIndicatorView.DEFAULT_BLOCKER_BACKGROUND_COLOR = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: UIControlEvents.valueChanged)
        tableJaunt.addSubview(refreshControl) // not required when using UITableViewController
        
        loadData()
    }
    
    @objc func refresh(sender: UIRefreshControl) {
        self.objectId.removeAll()
        self.painMoveStay.removeAll()
        self.advisorUserId.removeAll()
        self.painBetterWorse.removeAll()
        self.isAnswered.removeAll()
        self.currentMedications.removeAll()
        self.howLongResponse.removeAll()
        self.chargeId.removeAll()
        self.releventInfoResponse.removeAll()
        self.symptomCauses.removeAll()
        self.painResponse.removeAll()
        self.healthImage.removeAll()
        
        loadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if !newQuestion{
            if let indexPath = tableJaunt.indexPathForSelectedRow{
                let desti = segue.destination as! QuestionViewController
                desti.objectId = objectId[indexPath.row]
                desti.advisorUserId = advisorUserId[indexPath.row]
                desti.isAnswered = isAnswered[indexPath.row]
                desti.chargeId = chargeId[indexPath.row]

                desti.painMoveStay = painMoveStay[indexPath.row]
                desti.painBetterWorse = painBetterWorse[indexPath.row]
                desti.currentMedications = currentMedications[indexPath.row]
                desti.howLongResponse = howLongResponse[indexPath.row]
                desti.releventInfoResponse = releventInfoResponse[indexPath.row]
                desti.symptomCauses = symptomCauses[indexPath.row]
                desti.painResponse = painResponse[indexPath.row]
                desti.retrievedHealthImage = healthImage[indexPath.row]
                
                desti.advisorResponse = advisorResponse[indexPath.row]
                desti.advisorLevel = advisorLevel[indexPath.row]
                
                desti.isPosted = true
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if objectId.count > 0{
            return self.objectId.count

        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: MainTableViewCell!
        
        if objectId.count > 0{
            cell = tableView.dequeueReusableCell(withIdentifier: "myQuestionsReuse", for: indexPath) as! MainTableViewCell
            
            cell.selectionStyle = .none
            tableView.separatorStyle = .singleLine
            //cell.questionImage.kf.setImage(with: URL(string: questionImages[indexPath.row]), placeholder: UIImage(named: "appy"))
            //cell.questionImage.kf.setImage(with: URL(string: questionImages[indexPath.row]), for: .)
           // cell.questionLabel.text = questions[indexPath.row]
            //cell.statusLabel.text = questionStatus[indexPath.row]
            
            if isAnswered[indexPath.row]{
                let query = PFQuery(className:"_User")
                query.getObjectInBackground(withId: self.advisorUserId[indexPath.row]) {
                    (object: PFObject!, error: Error?) -> Void in
                    if error == nil && object != nil {
                        let image = object!["Profile"] as! PFFile
                        cell.userImage.kf.setImage(with: URL(string: image.url!), placeholder: UIImage(named: "appy"))
                        cell.userName.text = object!["DisplayName"] as? String
                        cell.bodyLabel.text = self.advisorLevel[indexPath.row]
                    }
                }
                
            } else {
                cell.userImage.image = UIImage(named: "angelina")
                cell.userName.text = "Angelina"
                cell.bodyLabel.text = "Thanks! Your health concern is pending a response from a U.S. registered nurse. He or she will reply to you ASAP."
            }
            
            if indexPath.row == objectId.count - 1{
                tableView.separatorStyle = .none
                
            }
            
           /* if isAnswered[indexPath.row]{
                cell.questionView.backgroundColor = uicolorFromHex(0x006a52)
                
            } else {
                cell.questionView.backgroundColor = uicolorFromHex(0x180d22)
            }*/
            
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: "noQuestionsReuse", for: indexPath) as! MainTableViewCell
            cell.selectionStyle = .none
            self.tableJaunt.separatorStyle = .none
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        newQuestion = false 
        performSegue(withIdentifier: "showQuestion", sender: self)
    }
    
    @IBAction func showQuestion(_ sender: UIBarButtonItem) {
        newQuestion = true
        performSegue(withIdentifier: "showQuestion", sender: self)
    }
    
    @IBAction func showMenu(_ sender: UIBarButtonItem) {
        if let container = self.so_containerViewController {
            container.isSideViewControllerPresented = true
        }
    }
    
    //data
    func loadData(){
        let query = PFQuery(className:"Question")
        query.whereKey("userId", equalTo: PFUser.current()!.objectId!)
        query.whereKey("isRemoved", equalTo: false)
        query.addDescendingOrder("createdAt")
        query.findObjectsInBackground {
            (objects: [PFObject]?, error: Error?) -> Void in
            if error == nil {
                if let objects = objects {
                    for object in objects {
                        self.objectId.append(object.objectId!)
                        self.chargeId.append(object["chargeId"] as! String)
                        self.isAnswered.append(object["isAnswered"] as! Bool)

                        self.painMoveStay.append(object["painMoveStay"] as! String)
                        self.painBetterWorse.append(object["painBetterWorse"] as! String)
                        self.currentMedications.append(object["currentMedications"] as! String)
                        self.howLongResponse.append(object["howLongResponse"] as! String)
                        self.releventInfoResponse.append(object["releventInfoResponse"] as! String)
                        self.symptomCauses.append(object["symptomCauses"] as! String)
                        self.painResponse.append(object["painResponse"] as! String)
                        //let url = object["videoScreenShot"] as! PFFile
                        
                        //user is not required to upload health image, so check to make sure this field isn't nil
                        if let healthImage: PFFile = object["healthImage"] as? PFFile{
                            self.healthImage.append(healthImage.url!)
                            
                        } else {
                            self.healthImage.append("")
                        }
                        
                        self.advisorUserId.append(object["advisorUserId"] as! String)
                        self.advisorLevel.append(object["level"] as! String)
                        self.advisorResponse.append(object["response"] as! String)
                        
                    }
                    
                    self.tableJaunt.reloadData()
                    self.refreshControl.endRefreshing()
                    self.stopAnimating()

                }
            } else {
                // Log details of the failure
                print("Error: \(error!)")
            }
        }
    }
    
    func uicolorFromHex(_ rgbValue:UInt32)->UIColor{
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        
        return UIColor(red:red, green:green, blue:blue, alpha:1.0)
    }
}
