//
//  DetailViewController.swift
//  Celecare
//
//  Created by Dominic Smith on 12/5/17.
//  Copyright © 2017 Celecare LLC. All rights reserved.
//

import UIKit

import UIKit
import SlackTextViewController
import Parse
import NVActivityIndicatorView
import SCLAlertView



class DetailViewController: SLKTextViewController,NVActivityIndicatorViewable {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.tableView?.register(ViewAnswerTableViewCell.self, forCellReuseIdentifier: "questionReuse")
       // self.tableView?.register(AdvisorTableViewCell.self, forCellReuseIdentifier: "imageReuse")
        self.tableView?.estimatedRowHeight = 50
        self.tableView?.rowHeight = UITableViewAutomaticDimension
        self.tableView?.separatorStyle = .none
        
        self.isInverted = false
        self.textView.placeholder = "Explain your concern in detail"
        self.rightButton.setTitle("Add", for: .normal)
        self.shouldScrollToBottomAfterKeyboardShows = true
        /*self.setTextInputbarHidden(true, animated: false)
        self.textInputbar.bringSubview(toFront: self.rightButton)
        self.textInputbar.bringSubview(toFront: self.textView)*/
        
        NVActivityIndicatorView.DEFAULT_TYPE = .ballScaleMultiple
      //  NVActivityIndicatorView.DEFAULT_COLOR = uicolorFromHex(0x006a52)
        NVActivityIndicatorView.DEFAULT_BLOCKER_SIZE = CGSize(width: 60, height: 60)
        NVActivityIndicatorView.DEFAULT_BLOCKER_BACKGROUND_COLOR = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
