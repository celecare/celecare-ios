//
//  MainSidebarViewController.swift
//  Celecare
//
//  Created by Dominic Smtih on 7/19/17.
//  Copyright © 2017 Sickcall LLC All rights reserved.
//

import UIKit
import Parse
import Kingfisher

class MainSidebarViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableJaunt: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableJaunt.register(MainTableViewCell.self, forCellReuseIdentifier: "sideBarReuse")
        self.tableJaunt.estimatedRowHeight = 50
        self.tableJaunt.rowHeight = UITableViewAutomaticDimension
        tableJaunt.separatorStyle = .singleLine
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "sideBarReuse", for: indexPath) as! MainTableViewCell
        cell.selectionStyle = .none

        cell.sideBarButton.addTarget(self, action: #selector(self.buttonAction(_:)), for: .touchUpInside)
        if indexPath.row == 0{
            cell.sideBarButton.tag = 0
            cell.sideBarButton.setTitle(" Sign out", for: .normal)
            cell.sideBarButton.setImage(UIImage(named: "signout"), for: .normal)
            
        } else if indexPath.row == 1{
            cell.sideBarButton.tag = 1
            cell.sideBarButton.setTitle(" Support", for: .normal)
            cell.sideBarButton.setImage(UIImage(named: "support"), for: .normal)
            
        } else if indexPath.row == 2{
            cell.sideBarButton.tag = 2
            cell.sideBarButton.setTitle(" Share Sickcall", for: .normal)
            cell.sideBarButton.setImage(UIImage(named: "share-icon"), for: .normal)
            tableView.separatorStyle = .none
        }

        return cell
    }
    
    @objc func buttonAction(_ sender: UIButton){
        if sender.tag == 0{
            PFUser.logOut()
            let storyboard = UIStoryboard(name: "Login", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "welcome")
            self.present(controller, animated: true, completion: nil)
            
        } else if sender.tag == 1{
            let url = URL(string : "https://www.sickcallhealth.com/advisor/app" )
            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
            
        } else {
            let textItem = "Find out how serious your health concern is through Sickcall!"
            let linkItem : NSURL = NSURL(string: "https://www.sickcallhealth.com/app")!
            // If you want to put an image
            
            let activityViewController : UIActivityViewController = UIActivityViewController(
                activityItems: [linkItem, textItem], applicationActivities: nil)
            
            self.present(activityViewController, animated: true, completion: nil)
        }
    }
}
