//
//  QuestionViewController.swift
//  Celecare
//
//  Created by Dominic Smith on 12/6/17.
//  Copyright © 2017 Celecare LLC. All rights reserved.
//

//TODO: load question data
//TODO: Parse live query

import UIKit
import Parse
import Kingfisher
import SlackTextViewController
import SCLAlertView
import NVActivityIndicatorView
import Stripe
import Alamofire
import SwiftyJSON

class QuestionViewController: SLKTextViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate,
NVActivityIndicatorViewable, STPAddCardViewControllerDelegate{
    
    let imagePicker = UIImagePickerController()
    
    var questionURL = "https://celecare.herokuapp.com/posts/assignQuestion"
    
    //determine what part of question usee is on
    var rows = 2
    
    //var freeze screen
    
    
    //Question values
    var objectId: String!
    
    //patient responses
    var userName = "Sickcaller"
    var currentMedications = ""
    var symptomCauses = ""
    var response2 = "Response 2"
    var howLongResponse = ""
    var painResponse = ""
    var painBetterWorse = ""
    var painMoveStay = ""
    var releventInfoResponse = ""
    var healthImage: UIImage!
    var retrievedHealthImage: String! 
    var pffile: PFFile!
    var uploadPic = false
    
    //advisor
    var advisorUserId: String!
    var advisorUsername: String!
    var advisorImage: String!
    var advisorLevel: String!
    var advisorResponse: String!
    
    
    var isAnswered = false
    
    //
    var isPosted = false
    var postFailed = false
    
    //payments
    var price = 0
    var priceURL = "https://celecare.herokuapp.com/payments"
    //var baseURL = "https://celecare.herokuapp.com/payments/createCharge"
    var chargeURL = "https://celecare.herokuapp.com/payments/createTestCharge"
    var chargeId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        
        if retrievedHealthImage != ""{
            uploadPic = true
        }
        loadPrice()
        
        //self.tableView?.register(ViewAnswerTableViewCell.self, forCellReuseIdentifier: "questionReuse")
        self.tableView?.register(NewQuestionTableViewCell.self, forCellReuseIdentifier: "botReuse")
        self.tableView?.register(NewQuestionTableViewCell.self, forCellReuseIdentifier: "botButtonReuse")
        self.tableView?.register(NewQuestionTableViewCell.self, forCellReuseIdentifier: "patientReuse")
        self.tableView?.register(NewQuestionTableViewCell.self, forCellReuseIdentifier: "healthImageReuse")
        self.tableView?.register(NewQuestionTableViewCell.self, forCellReuseIdentifier: "loadingReuse")
        self.tableView?.register(NewQuestionTableViewCell.self, forCellReuseIdentifier: "advisorReuse")
        self.tableView?.register(NewQuestionTableViewCell.self, forCellReuseIdentifier: "seriousReuse")
        self.tableView?.estimatedRowHeight = 50
        self.tableView?.rowHeight = UITableViewAutomaticDimension
        self.tableView?.separatorStyle = .none
        
        //slackview settings
        self.isInverted = false
        self.textView.placeholder = "Respond"
        self.rightButton.setTitle("Send", for: .normal)
        self.shouldScrollToBottomAfterKeyboardShows = true
        
        NVActivityIndicatorView.DEFAULT_TYPE = .ballScaleMultiple
        NVActivityIndicatorView.DEFAULT_COLOR = uicolorFromHex(0x006a52)
        NVActivityIndicatorView.DEFAULT_BLOCKER_SIZE = CGSize(width: 60, height: 60)
        NVActivityIndicatorView.DEFAULT_BLOCKER_BACKGROUND_COLOR = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        
        /*self.setTextInputbarHidden(true, animated: false)
         self.textInputbar.bringSubview(toFront: self.rightButton)
         self.textInputbar.bringSubview(toFront: self.textView)*/
        
      /*  NVActivityIndicatorView.DEFAULT_TYPE = .ballScaleMultiple
        //  NVActivityIndicatorView.DEFAULT_COLOR = uicolorFromHex(0x006a52)
        NVActivityIndicatorView.DEFAULT_BLOCKER_SIZE = CGSize(width: 60, height: 60)
        NVActivityIndicatorView.DEFAULT_BLOCKER_BACKGROUND_COLOR = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)*/
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rows
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: NewQuestionTableViewCell!
        switch indexPath.row{
        case 0:
            cell = tableView.dequeueReusableCell(withIdentifier: "botReuse", for: indexPath) as! NewQuestionTableViewCell
            cell.selectionStyle = .none
            cell.botQuestion.text = "Hey Sickcaller! I'm Angelina, your Sickcall helper bot! I'm going to ask you a few questions that will help your nurse advisor accurately answer your question."
            break
            
        case 1:
            cell = tableView.dequeueReusableCell(withIdentifier: "botReuse", for: indexPath) as! NewQuestionTableViewCell
            cell.selectionStyle = .none
            cell.botQuestion.text = "Are you currently taking any over the counter or presciption medicines? If yes, name them please."
            break
            
            //current medications
        case 2:
            cell = tableView.dequeueReusableCell(withIdentifier: "patientReuse", for: indexPath) as! NewQuestionTableViewCell
            cell.selectionStyle = .none
            cell.patientResponse.text = currentMedications
            break
            
        case 3:
            cell = tableView.dequeueReusableCell(withIdentifier: "botReuse", for: indexPath) as! NewQuestionTableViewCell
            cell.selectionStyle = .none
            cell.botQuestion.text = "What might be the cause of your symptoms, trauma-wise? If you don't know, what were you doing when you noticed your symptoms?"
            break
            
            //symptom Causes
        case 4:
            cell = tableView.dequeueReusableCell(withIdentifier: "patientReuse", for: indexPath) as! NewQuestionTableViewCell
            cell.selectionStyle = .none
            cell.patientResponse.text = symptomCauses
            break
            
        case 5:
            cell = tableView.dequeueReusableCell(withIdentifier: "botReuse", for: indexPath) as! NewQuestionTableViewCell
            cell.selectionStyle = .none
            cell.botQuestion.text = "How long has this health concern been bothering you?"
            break
            
        case 6:
            cell = tableView.dequeueReusableCell(withIdentifier: "botButtonReuse", for: indexPath) as! NewQuestionTableViewCell
            cell.selectionStyle = .none
            cell.botButton.setTitle("Today", for: .normal)
            cell.botButton.addTarget(self, action: #selector(self.buttonAction(_:)), for: .touchUpInside)
            cell.botButton.tag = 0
            break
            
        case 7:
            cell = tableView.dequeueReusableCell(withIdentifier: "botButtonReuse", for: indexPath) as! NewQuestionTableViewCell
            cell.selectionStyle = .none
            cell.botButton.setTitle("Yesterday", for: .normal)
            cell.botButton.addTarget(self, action: #selector(self.buttonAction(_:)), for: .touchUpInside)
            cell.botButton.tag = 1
            break
            
        case 8:
            cell = tableView.dequeueReusableCell(withIdentifier: "botButtonReuse", for: indexPath) as! NewQuestionTableViewCell
            cell.selectionStyle = .none
            cell.botButton.setTitle("This past week", for: .normal)
            cell.botButton.addTarget(self, action: #selector(self.buttonAction(_:)), for: .touchUpInside)
            cell.botButton.tag = 2
            break
            
        case 9:
            cell = tableView.dequeueReusableCell(withIdentifier: "botButtonReuse", for: indexPath) as! NewQuestionTableViewCell
            cell.selectionStyle = .none
            cell.botButton.setTitle("This past month", for: .normal)
            cell.botButton.addTarget(self, action: #selector(self.buttonAction(_:)), for: .touchUpInside)
            cell.botButton.tag = 3
            break
            
        case 10:
            cell = tableView.dequeueReusableCell(withIdentifier: "botButtonReuse", for: indexPath) as! NewQuestionTableViewCell
            cell.selectionStyle = .none
            cell.botButton.setTitle("This past year", for: .normal)
            cell.botButton.addTarget(self, action: #selector(self.buttonAction(_:)), for: .touchUpInside)
            cell.botButton.tag = 4
            break
            
        case 11:
            cell = tableView.dequeueReusableCell(withIdentifier: "botButtonReuse", for: indexPath) as! NewQuestionTableViewCell
            cell.selectionStyle = .none
            cell.botButton.setTitle("More than a year", for: .normal)
            cell.botButton.addTarget(self, action: #selector(self.buttonAction(_:)), for: .touchUpInside)
            cell.botButton.tag = 5
            break
            
        case 12:
            cell = tableView.dequeueReusableCell(withIdentifier: "patientReuse", for: indexPath) as! NewQuestionTableViewCell
            cell.selectionStyle = .none
            cell.patientResponse.text = howLongResponse
            break
            
        case 13:
            cell = tableView.dequeueReusableCell(withIdentifier: "botReuse", for: indexPath) as! NewQuestionTableViewCell
            cell.selectionStyle = .none
            cell.botQuestion.text = "Are you experiencing any pain?"
            break
            
        case 14:
            cell = tableView.dequeueReusableCell(withIdentifier: "botButtonReuse", for: indexPath) as! NewQuestionTableViewCell
            cell.selectionStyle = .none
            cell.botButton.setTitle("none", for: .normal)
            cell.botButton.addTarget(self, action: #selector(self.buttonAction(_:)), for: .touchUpInside)
            cell.botButton.tag = 6
            break
            
        case 15:
            cell = tableView.dequeueReusableCell(withIdentifier: "botButtonReuse", for: indexPath) as! NewQuestionTableViewCell
            cell.selectionStyle = .none
            cell.botButton.setTitle("Burning", for: .normal)
            cell.botButton.addTarget(self, action: #selector(self.buttonAction(_:)), for: .touchUpInside)
            cell.botButton.tag = 7
            break
            
        case 16:
            cell = tableView.dequeueReusableCell(withIdentifier: "botButtonReuse", for: indexPath) as! NewQuestionTableViewCell
            cell.selectionStyle = .none
            cell.botButton.setTitle("Stabbing", for: .normal)
            cell.botButton.addTarget(self, action: #selector(self.buttonAction(_:)), for: .touchUpInside)
            cell.botButton.tag = 8
            break
            
        case 17:
            cell = tableView.dequeueReusableCell(withIdentifier: "botButtonReuse", for: indexPath) as! NewQuestionTableViewCell
            cell.selectionStyle = .none
            cell.botButton.setTitle("Dull", for: .normal)
            cell.botButton.addTarget(self, action: #selector(self.buttonAction(_:)), for: .touchUpInside)
            cell.botButton.tag = 9
            break
            
        case 18:
            cell = tableView.dequeueReusableCell(withIdentifier: "patientReuse", for: indexPath) as! NewQuestionTableViewCell
            cell.selectionStyle = .none
            cell.patientResponse.text = painResponse
            break
            
        case 19:
            if painResponse == "None"{
                cell = tableView.dequeueReusableCell(withIdentifier: "botReuse", for: indexPath) as! NewQuestionTableViewCell
                cell.selectionStyle = .none
                cell.botQuestion.text = "Any other relevant information that you would like to provide?"
                
            } else {
                cell = tableView.dequeueReusableCell(withIdentifier: "botReuse", for: indexPath) as! NewQuestionTableViewCell
                cell.selectionStyle = .none
                cell.botQuestion.text = "Does anything make your pain better or worse?"
            }

            break
            
        case 20:
            cell = tableView.dequeueReusableCell(withIdentifier: "patientReuse", for: indexPath) as! NewQuestionTableViewCell
            cell.selectionStyle = .none
            if painResponse == "None"{
                cell.patientResponse.text = releventInfoResponse
                
            } else {
                cell.patientResponse.text = painBetterWorse
            }
            
            break
            
        case 21:
            if painResponse == "None"{
                cell = tableView.dequeueReusableCell(withIdentifier: "botReuse", for: indexPath) as! NewQuestionTableViewCell
                cell.selectionStyle = .none
                cell.botQuestion.text = "Would you like to add an image that shows your health concern?"
                
            } else {
                cell = tableView.dequeueReusableCell(withIdentifier: "botReuse", for: indexPath) as! NewQuestionTableViewCell
                cell.selectionStyle = .none
                cell.botQuestion.text = "Does the pain move or stay in the same spot?"
            }
            
        case 22:
            if painResponse == "None"{
                cell = tableView.dequeueReusableCell(withIdentifier: "botButtonReuse", for: indexPath) as! NewQuestionTableViewCell
                cell.selectionStyle = .none
                cell.botButton.setTitle("Yes", for: .normal)
                cell.botButton.addTarget(self, action: #selector(self.buttonAction(_:)), for: .touchUpInside)
                cell.botButton.tag = 10
                
            } else {
                cell = tableView.dequeueReusableCell(withIdentifier: "patientReuse", for: indexPath) as! NewQuestionTableViewCell
                cell.selectionStyle = .none
                
               cell.patientResponse.text = painMoveStay
            }
            
            break
            
        case 23:
            if painResponse == "None"{
                if uploadPic{
                    cell = tableView.dequeueReusableCell(withIdentifier: "healthImageReuse", for: indexPath) as! NewQuestionTableViewCell
                    cell.selectionStyle = .none
                    
                    if isPosted{
                        cell.patientHealthImage.kf.setImage(with: URL(string: retrievedHealthImage), placeholder: UIImage(named: "appy"))
                    } else {
                        cell.patientHealthImage.image = healthImage
                    }
                    
                } else {
                    cell = tableView.dequeueReusableCell(withIdentifier: "botButtonReuse", for: indexPath) as! NewQuestionTableViewCell
                    cell.selectionStyle = .none
                    cell.botButton.setTitle("No", for: .normal)
                    cell.botButton.addTarget(self, action: #selector(self.buttonAction(_:)), for: .touchUpInside)
                    cell.botButton.tag = 11
                }
                
            } else {
                
                cell = tableView.dequeueReusableCell(withIdentifier: "botReuse", for: indexPath) as! NewQuestionTableViewCell
                cell.selectionStyle = .none
                cell.botQuestion.text = "Any other relevant information that you would like to provide?"
            }
            
        case 24:
            if painResponse == "None"{
                if isPosted{
                    cell = tableView.dequeueReusableCell(withIdentifier: "botReuse", for: indexPath) as! NewQuestionTableViewCell
                    cell.selectionStyle = .none
                    cell.botQuestion.text = "Thanks! Your health concern is pending a response from a U.S. registered nurse. He or she will reply to you ASAP."
                    
                } else if postFailed {
                    cell = tableView.dequeueReusableCell(withIdentifier: "botButtonReuse", for: indexPath) as! NewQuestionTableViewCell
                    cell.selectionStyle = .none
                    cell.botButton.setTitle("Try again", for: .normal)
                    cell.botButton.tag = 12
                    
                } else {
                    cell = tableView.dequeueReusableCell(withIdentifier: "loadingReuse", for: indexPath) as! NewQuestionTableViewCell
                    cell.activityIndicator.startAnimating()
                }
        
            } else {
                
                cell = tableView.dequeueReusableCell(withIdentifier: "patientReuse", for: indexPath) as! NewQuestionTableViewCell
                cell.selectionStyle = .none
                
                cell.patientResponse.text = releventInfoResponse
            }

            break
            
        case 25:
            if painResponse == "None"{
                cell = tableView.dequeueReusableCell(withIdentifier: "seriousReuse", for: indexPath) as! NewQuestionTableViewCell
                //replace with actual response
                cell.selectionStyle = .none
                cell.advisorSeriousLevel.backgroundColor = .red
                cell.advisorSeriousLevel.text = advisorLevel
                loadAdvisor(cell: cell)
                
            } else{
                cell = tableView.dequeueReusableCell(withIdentifier: "botReuse", for: indexPath) as! NewQuestionTableViewCell
                cell.selectionStyle = .none
                cell.botQuestion.text = "Would you like to add an image that shows your health concern?"
            }

            break
            
        case 26:
            if painResponse == "None"{
                cell = tableView.dequeueReusableCell(withIdentifier: "botReuse", for: indexPath) as! NewQuestionTableViewCell
                cell.selectionStyle = .none
                cell.botQuestion.text = "You got a reply! Find out what your advisor has to say about your health concern."
                
            } else {
                cell = tableView.dequeueReusableCell(withIdentifier: "botButtonReuse", for: indexPath) as! NewQuestionTableViewCell
                cell.selectionStyle = .none
                cell.botButton.setTitle("Yes", for: .normal)
                cell.botButton.tag = 10
            }
            break
            
        case 27:
            if painResponse == "None"{
                cell = tableView.dequeueReusableCell(withIdentifier: "botButtonReuse", for: indexPath) as! NewQuestionTableViewCell
                cell.selectionStyle = .none
                let price = Double(self.price) * 0.01
                let stringPrice = String(format:"%.2f", price)
                cell.botButton.setTitle("Pay $\(stringPrice)", for: .normal)
                cell.botButton.tag = 13
                
            } else {
                cell = tableView.dequeueReusableCell(withIdentifier: "botButtonReuse", for: indexPath) as! NewQuestionTableViewCell
                cell.selectionStyle = .none
                cell.botButton.setTitle("no", for: .normal)
                cell.botButton.tag = 11
            }
            
            break
            
        case 28:
            if painResponse == "None"{
                cell = tableView.dequeueReusableCell(withIdentifier: "advisorReuse", for: indexPath) as! NewQuestionTableViewCell
                loadAdvisor(cell: cell)
                cell.advisorResponse.text = advisorResponse
                cell.selectionStyle = .none
                
            } else if uploadPic{
                cell = tableView.dequeueReusableCell(withIdentifier: "healthImageReuse", for: indexPath) as! NewQuestionTableViewCell
                cell.selectionStyle = .none
                
                if isPosted{
                    cell.patientHealthImage.kf.setImage(with: URL(string: retrievedHealthImage), placeholder: UIImage(named: "appy"))
                } else {
                    cell.patientHealthImage.image = healthImage
                }
                
            } else if isPosted{
                cell = tableView.dequeueReusableCell(withIdentifier: "botReuse", for: indexPath) as! NewQuestionTableViewCell
                cell.selectionStyle = .none
                cell.botQuestion.text = "Thank you for your responses! Your advisor will respond ASAP."
                    
            } else if postFailed {
                cell = tableView.dequeueReusableCell(withIdentifier: "botButtonReuse", for: indexPath) as! NewQuestionTableViewCell
                cell.selectionStyle = .none
                cell.botButton.setTitle("Try again", for: .normal)
                cell.botButton.tag = 12
                
            }else {
                cell = tableView.dequeueReusableCell(withIdentifier: "loadingReuse", for: indexPath) as! NewQuestionTableViewCell
                cell.activityIndicator.startAnimating()
            }
            break
            
        case 29:
            if isPosted{
                cell = tableView.dequeueReusableCell(withIdentifier: "botReuse", for: indexPath) as! NewQuestionTableViewCell
                cell.selectionStyle = .none
                cell.botQuestion.text = "Thank you for your responses! Your advisor will respond ASAP."
                
            } else if postFailed {
                cell = tableView.dequeueReusableCell(withIdentifier: "botButtonReuse", for: indexPath) as! NewQuestionTableViewCell
                cell.selectionStyle = .none
                cell.botButton.setTitle("Try again", for: .normal)
                cell.botButton.tag = 12
                
            } else {
                cell = tableView.dequeueReusableCell(withIdentifier: "loadingReuse", for: indexPath) as! NewQuestionTableViewCell
                cell.activityIndicator.startAnimating()
            }

            break
            
            //nurse advisor response
            
        case 30:
            cell = tableView.dequeueReusableCell(withIdentifier: "seriousReuse", for: indexPath) as! NewQuestionTableViewCell
            //replace with actual response
            cell.selectionStyle = .none
            //cell.advisorSeriousLevel.backgroundColor = .red
            cell.advisorSeriousLevel.text = advisorLevel
            loadAdvisor(cell: cell)
            
            break
            
        case 31:
            cell = tableView.dequeueReusableCell(withIdentifier: "botReuse", for: indexPath) as! NewQuestionTableViewCell
            cell.selectionStyle = .none
            cell.botQuestion.text = "You got a reply! Find out what your advisor has to say about your health concern."
            
            break
            
        case 32:
            cell = tableView.dequeueReusableCell(withIdentifier: "botButtonReuse", for: indexPath) as! NewQuestionTableViewCell
            cell.selectionStyle = .none
            let price = Double(self.price) * 0.01
            let stringPrice = String(format:"%.2f", price)
            cell.botButton.setTitle("Pay $\(stringPrice)", for: .normal)
            cell.botButton.tag = 13
            break
            
        case 33:
            cell = tableView.dequeueReusableCell(withIdentifier: "advisorReuse", for: indexPath) as! NewQuestionTableViewCell
            loadAdvisor(cell: cell)
            cell.advisorResponse.text = advisorResponse
            cell.selectionStyle = .none
            break
            
        case 34:
            //bot questions about feedback
            
            break
            
        case 35:
            //yes button
            break
            
        case 36:
            //no butotn
            break
            
        case 37:
            //if yes, bot thank you
            //if no, bot ask why
            break
            
        case 38:
            //user response
            break
            
        case 39:
            //bot response
            break
            
        default:
           cell = tableView.dequeueReusableCell(withIdentifier: "", for: indexPath) as! NewQuestionTableViewCell
           cell.selectionStyle = .none
            break
        }
        
        return cell
    }

    @objc func buttonAction(_ sender: UIButton){
        //if statement is to stop users from being able to press buttons after they have posted their question
        if !isPosted{
            switch sender.tag {
            //how long has concern been bothering you actions
            case 0:
                howLongResponse = "Today"
                rows = 18
                break
                
            case 1:
                howLongResponse = "Yesterday"
                rows = 18
                break
                
            case 2:
                howLongResponse = "This past week"
                rows = 18
                break
                
            case 3:
                howLongResponse = "This past month"
                rows = 18
                break
                
            case 4:
                howLongResponse = "This past year"
                rows = 18
                break
                
            case 5:
                howLongResponse = "More than a year"
                rows = 18
                break
                
            //do you have pain actions
            case 6:
                painResponse = "None"
                rows = 20
                break
                
            case 7:
                painResponse = "Burning"
                rows = 20
                break
                
            case 8:
                painResponse = "Stabbing"
                rows = 20
                break
                
            case 9:
                painResponse = "Dull"
                rows = 20
                break
                
            //yes to uploading picture
            case 10:
                self.setTextInputbarHidden(true, animated: false)
                imagePicker.allowsEditing = false
                imagePicker.sourceType = .camera
                present(imagePicker, animated: true, completion: nil)
                break
                
            //no to uploading picture
            case 11:
                self.setTextInputbarHidden(true, animated: true)
                if painResponse == "None"{
                    self.rows = 25
                    
                } else {
                    self.rows = 29
                    
                }
                self.postIt(isPhoto: false)
                break
                
            //try post again
            case 12:
                postFailed = false
                
                if painResponse == "None"{
                    self.rows = 25
                    
                } else {
                    if uploadPic{
                        self.rows = 30
                        
                    } else {
                        self.rows = 29
                    }
                }
                
                if uploadPic{
                    self.pffile?.saveInBackground {
                        (success: Bool, error: Error?) -> Void in
                        if (success) {
                            self.postIt(isPhoto: true)
                            
                        } else {
                            self.postFailed = true
                            self.showPostResult(isPosted: false)
                        }
                    }
                    
                } else {
                    self.postIt(isPhoto: uploadPic)
                }
                
                break
                
            default:
                break
            }
            
            self.tableView?.reloadData()
            self.scrollDown()
        }
        
        if chargeId == "" && sender.tag == 13{
            let addCardViewController = STPAddCardViewController()
            addCardViewController.delegate = self
            // STPAddCardViewController must be shown inside a UINavigationController.
            let navigationController = UINavigationController(rootViewController: addCardViewController)
            self.present(navigationController, animated: true, completion: nil)
            self.tableView?.reloadData()
            self.scrollDown()
        }
    }
    
    override func didPressRightButton(_ sender: Any?) {
        switch rows{
        case 2:
            currentMedications = self.textView.text
            rows = 4
            break
            
        case 4:
            symptomCauses = self.textView.text
            rows = 12
            break
            
        case 20:
            if painResponse == "None"{
                rows = 24
                releventInfoResponse = self.textView.text
                
            } else {
                rows = 22
                painBetterWorse = self.textView.text
            }
            break
            
        case 22:
            rows = 24
            if painResponse == "None"{
                
            } else {
                painMoveStay = self.textView.text
            }
            break
            
        case 24:
            if painResponse == "None"{
                rows = 25
                
            } else {
                rows = 28
                releventInfoResponse = self.textView.text
            }
            
            break
            
        default :
            break
        }
        self.textView.text = ""
        self.tableView?.reloadData()
        self.scrollDown()
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        dismiss(animated: true, completion: nil)
        self.uploadPic = true
        self.healthImage = chosenImage
        if painResponse == "None"{
            self.rows = 25
            
        } else {
            self.rows = 30
        }
        
        self.tableView?.reloadData()
        self.scrollDown()
        
        let proPic = UIImageJPEGRepresentation(chosenImage, 0.5)
        self.pffile = PFFile(name: "healthImage.jpeg", data: proPic!)
        self.pffile?.saveInBackground {
            (success: Bool, error: Error?) -> Void in
            if (success) {
                self.postIt(isPhoto: true)
                
            } else {
                self.postFailed = true
                self.showPostResult(isPosted: false)
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func postIt(isPhoto: Bool){
        let newQuestion = PFObject(className: "Question")
        newQuestion["userId"] = PFUser.current()?.objectId
        newQuestion["advisorUserId"] = ""
        newQuestion["currentMedications"] = currentMedications
        newQuestion["symptomCauses"] = symptomCauses
        newQuestion["howLongResponse"] = howLongResponse
        newQuestion["painResponse"] = painResponse
        newQuestion["painBetterWorse"] = painBetterWorse
        newQuestion["painMoveStay"] = painMoveStay
        newQuestion["releventInfoResponse"] = releventInfoResponse
        newQuestion["isAnswered"] = false
        newQuestion["isRemoved"] = false
        newQuestion["chargeId"] = ""
        newQuestion["response"] = ""
        newQuestion["level"] = ""
        if isPhoto{
            newQuestion["healthImage"] = pffile
        }
        newQuestion.saveEventually{
            (success: Bool, error: Error?) -> Void in
            if (success) {
                self.assignQuestion(objectId: newQuestion.objectId!)
                self.postFailed = false
                self.showPostResult(isPosted: true)
                self.objectId = newQuestion.objectId!
                
            } else {
                print(error!)
                self.postFailed = true
                self.showPostResult(isPosted: false)
                //SCLAlertView().showError("Post UnSuccessful", subTitle: "Check internet connection and try again.")
            }
        }
    }
    
    func showPostResult(isPosted: Bool){
        //TODO: uncomment when live.....
        self.isPosted = isPosted
        if self.uploadPic{
            
            if self.painResponse == "None"{
                
                self.rows = 25
            } else {
                self.rows = 30
            }
            
        } else {
            if self.painResponse == "None"{
                self.rows = 25
                
            } else {
                self.rows = 29
                
            }
        }
        
        //TODO: this skips ahead like user got a response... this code show go at the parse live query part
        //TODO: this code show also go where the data is intially loaded. If advisor responsds.. go to response.
       /* self.isPosted = isPosted
        if painResponse == "None"{
            self.rows = 28
            
        } else {
            self.rows = 33
        }*/
        
        self.tableView?.reloadData()
        self.scrollDown()
    }
    
    //assign quesiton to nurse advisor
    func assignQuestion(objectId: String){
        Alamofire.request(self.questionURL, method: .post, parameters: ["id": objectId], encoding: JSONEncoding.default).validate().response{response in
            print(response)
        }
    }
    
    //payments
    func addCardViewControllerDidCancel(_ addCardViewController: STPAddCardViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func addCardViewController(_ addCardViewController: STPAddCardViewController, didCreateToken token: STPToken, completion: @escaping STPErrorBlock) {
        self.createCharge(tokenId: token.tokenId)
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func createCharge(tokenId: String){
        let p: Parameters = [
            "total": price,
            "description": "\(String(describing: PFUser.current()!.email!))'s Sickcall",
            "token": tokenId,
            "email": PFUser.current()!.email!
        ]
        
        Alamofire.request(self.chargeURL, method: .post, parameters: p, encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)
                //print("JSON: \(json)")
                
                if let status = json["statusCode"].int{
                    print(status)
                    let message = json["message"].string
                    SCLAlertView().showError("Something Went Wrong", subTitle: message!)
                    
                } else {
                    //successful charge
                    self.chargeId = json["id"].string!
                    self.saveChargeId()
                    
                    //show advisor response
                    if self.painResponse == "None"{
                        self.rows = 29
                        
                    } else {
                        self.rows = 34
                    }
                    
                    self.tableView?.reloadData()
                    self.scrollDown()
                }
                
            case .failure(let error):
                print(error)
                SCLAlertView().showError("Charge Unsuccessful", subTitle: error.localizedDescription)
            }
        }
    }
    
    func loadAdvisor(cell: NewQuestionTableViewCell!){
        let query = PFQuery(className: "Advisor")
        query.whereKey("userId", equalTo: advisorUserId )
        query.getFirstObjectInBackground {
            (object: PFObject!, error: Error?) -> Void in
            if error == nil || object != nil {
                cell.advisorState.text = object["state"] as? String
                let issueDate = object["originalIssueDate"] as! Date
                let issueYears = "\(Date.years(issueDate))"
                
                cell.advisorExperience.text = issueYears
                
                let userQuery = PFQuery(className:"_User")
                userQuery.getObjectInBackground(withId: self.advisorUserId ) {
                    (object: PFObject!, error: Error?) -> Void in
                    if error == nil && object != nil {
                        let image = object!["Profile"] as! PFFile
                        cell.advisorImage.kf.setImage(with: URL(string: image.url!), placeholder: UIImage(named: "appy"))
                        cell.advisorName.text = object!["DisplayName"] as? String
                        cell.advisorResponse.text = self.advisorResponse
                    }
                }
            }
        }
    }
    
    func loadPrice(){
        startAnimating()
        Alamofire.request(self.priceURL, method: .get, encoding: JSONEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success(let data):
                self.stopAnimating()
                let json = JSON(data)
                
                let booking_fee = json["booking_fee"].int!
                let nurse_fee = json["advisor_fee"].int!
                self.price = booking_fee + nurse_fee
                
                if self.chargeId != ""{
                    self.setTextInputbarHidden(true, animated: false)
                    if self.painResponse == "None"{
                        self.rows = 29
                        
                    } else {
                        self.rows = 34
                    }
                    
                } else if self.isAnswered{
                    self.setTextInputbarHidden(true, animated: false)
                    if self.painResponse == "None"{
                        self.rows = 28
                        
                    } else {
                        self.rows = 33
                    }
                    
                } else if self.isPosted{
                    self.setTextInputbarHidden(true, animated: false)
                    if self.uploadPic{
                        
                        if self.painResponse == "None"{
                            
                            self.rows = 25
                        } else {
                            self.rows = 30
                        }
                        
                    } else {
                        if self.painResponse == "None"{
                            self.rows = 25
                            
                        } else {
                            self.rows = 29
                            
                        }
                    }
                }
                self.tableView?.reloadData()
                self.scrollDown()
                
            case .failure(let error):
               // print(error)
                self.stopAnimating()
                let alert = UIAlertController(title: "Something Went Wrong", message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Reload", style: .default, handler: { action in
                    alert.dismiss(animated: true, completion: nil)
                    self.loadPrice()
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func saveChargeId(){
        let query = PFQuery(className: "Question")
        query.whereKey("objectId", equalTo: objectId )
        query.getFirstObjectInBackground {
            (object: PFObject?, error: Error?) -> Void in
            if error == nil || object != nil {
                object!["chargeId"] = self.chargeId
                object?.saveEventually()
                }
            }
    }

    //mich
    func uicolorFromHex(_ rgbValue:UInt32)->UIColor{
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        
        return UIColor(red:red, green:green, blue:blue, alpha:1.0)
    }
    
    func scrollDown(){
       DispatchQueue.main.async  {
            let indexPath = IndexPath(row: self.rows-1, section: 0)
            self.tableView?.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }
}

extension Date {
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
}
