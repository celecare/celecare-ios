//
//  SexViewController.swift
//  Celecare
//
//  Created by Dominic Smith on 12/15/17.
//  Copyright © 2017 Celecare LLC. All rights reserved.
//

import UIKit
import SCLAlertView
import NVActivityIndicatorView
import SnapKit

class SexViewController: UIViewController {
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "HelveticaNeue-Bold", size: 30)
        label.text = "What's your Gender?"
        label.textColor = UIColor.black
        label.numberOfLines = 0
        return label
    }()
    
    lazy var malebutton: UIButton = {
        let button = UIButton()
        button.setTitle("Male", for: .normal)
        button.titleLabel?.font = UIFont(name: "HelveticaNeue", size: 20)
        button.setTitleColor(.white, for: .normal)
        button.setBackgroundColor(uicolorFromHex(0x006a52), forState: .normal)
        button.titleLabel?.textAlignment = .center
        button.addTarget(self, action: #selector(self.buttonAction(sender:)), for: .touchUpInside)
        button.layer.cornerRadius = 3
        button.clipsToBounds = true
        button.tag = 0
        return button
    }()
    
    lazy var Femalebutton: UIButton = {
        let button = UIButton()
        button.setTitle("Female", for: .normal)
        button.titleLabel?.font = UIFont(name: "HelveticaNeue", size: 20)
        button.setTitleColor(.white, for: .normal)
        button.setBackgroundColor(uicolorFromHex(0x006a52), forState: .normal)
        button.titleLabel?.textAlignment = .center
        button.addTarget(self, action: #selector(self.buttonAction(sender:)), for: .touchUpInside)
        button.layer.cornerRadius = 3
        button.clipsToBounds = true 
        button.tag = 1
        return button
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(titleLabel)
        self.view.addSubview(malebutton)
        self.view.addSubview(Femalebutton)
        
        titleLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.view).offset(75)
            make.left.equalTo(self.view).offset(10)
            make.right.equalTo(self.view).offset(-10)
        }
        
        malebutton.snp.makeConstraints { (make) -> Void in
            make.height.equalTo(50)
            // make.top.equalTo(datePicker.snp.bottom)
            make.bottom.equalTo(self.Femalebutton.snp.top).offset(-20)
            make.left.equalTo(self.view).offset(10)
            make.right.equalTo(self.view).offset(-10)
        }
        
        Femalebutton.snp.makeConstraints { (make) -> Void in
            make.height.equalTo(50)
            // make.top.equalTo(datePicker.snp.bottom)
            make.bottom.equalTo(self.view).offset(-20)
            make.left.equalTo(self.view).offset(10)
            make.right.equalTo(self.view).offset(-10)
        }
        
    }
    
    @objc func buttonAction(sender: UIButton){
        
    }
    
    
    func uicolorFromHex(_ rgbValue:UInt32)->UIColor{
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        
        return UIColor(red:red, green:green, blue:blue, alpha:1.0)
    }
}
