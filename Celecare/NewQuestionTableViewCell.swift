//
//  NewQuestionTableViewCell.swift
//  Celecare
//
//  Created by Dominic Smith on 12/6/17.
//  Copyright © 2017 Celecare LLC. All rights reserved.
//

import UIKit

class NewQuestionTableViewCell: UITableViewCell {
    
    var pastelGreen  = UIColor(red: 205/255, green: 229/255, blue: 213/255, alpha: 1.0)
    var pastelBlue = UIColor(red: 218/255, green: 228/255, blue: 237/255, alpha: 1.0)
    var tan  = UIColor(red: 232/255, green: 230/255, blue: 223/255, alpha: 1.0)
    var gray = UIColor(red: 223/255, green: 225/255, blue: 232/255, alpha: 1.0)
        
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        if reuseIdentifier == "botReuse"{
            self.addSubview(botImage)
            //self.addSubview(botName)
            //self.addSubview(botQuestion)
            self.addSubview(botView)
            self.botView.addSubview(botQuestion)
            
            botImage.snp.makeConstraints { (make) -> Void in
                make.height.width.equalTo(30)
               // make.top.equalTo(self).offset(20)
                make.left.equalTo(self).offset(10)
                make.bottom.equalTo(self)
               // make.right.equalTo(self).offset(-50)
            }
            
           /* botName.snp.makeConstraints { (make) -> Void in
                make.top.equalTo(botImage).offset(8)
                make.left.equalTo(botImage.snp.right).offset(5)
                make.right.equalTo(self).offset(-20)
            }*/
            
            botView.snp.makeConstraints { (make) -> Void in
                make.top.equalTo(self).offset(5)
                make.left.equalTo(botImage.snp.right).offset(5)
                make.right.equalTo(self).offset(-50)
                make.bottom.equalTo(botImage.snp.top).offset(30)
            }
            
            botQuestion.snp.makeConstraints { (make) -> Void in
                make.top.equalTo(botView.snp.top).offset(10)
                make.left.equalTo(botView.snp.left).offset(10)
                make.right.equalTo(botView.snp.right).offset(-10)
                make.bottom.equalTo(botView.snp.bottom).offset(-10)
            }
            
        } else if reuseIdentifier == "botButtonReuse"{
            self.addSubview(botButton)
            
            botButton.snp.makeConstraints { (make) -> Void in
                make.height.equalTo(50)
                make.width.equalTo(200)
                make.top.equalTo(self).offset(10)
                make.right.equalTo(self).offset(-10)
                make.bottom.equalTo(self).offset(-5)
            }
            
        } else if reuseIdentifier == "patientReuse"{
            self.addSubview(patientView)
            self.patientView.addSubview(patientResponse)
            
            patientResponse.snp.makeConstraints { (make) -> Void in
                make.top.equalTo(patientView.snp.top).offset(10)
                make.left.equalTo(patientView.snp.left).offset(10)
                make.right.equalTo(patientView.snp.right).offset(-10)
                make.bottom.equalTo(patientView.snp.bottom).offset(-10)
            }
            
            patientView.snp.makeConstraints { (make) -> Void in
                make.top.equalTo(self).offset(5)
                make.left.equalTo(self).offset(100)
                make.right.equalTo(self).offset(-10)
                make.bottom.equalTo(self).offset(-5)
            }
                        
        } else if reuseIdentifier == "healthImageReuse" {
            self.addSubview(patientHealthImage)
            patientHealthImage.snp.makeConstraints { (make) -> Void in
                make.width.height.equalTo(200)
                make.top.equalTo(self).offset(5)
                //make.left.equalTo(self).offset(10)
                make.right.equalTo(self).offset(-5)
                make.bottom.equalTo(self).offset(-5)
            }
            
        } else if reuseIdentifier == "loadingReuse"{
            self.addSubview(activityIndicator)
            activityIndicator.snp.makeConstraints { (make) -> Void in
                make.top.equalTo(self).offset(5)
                //make.left.equalTo(self).offset(10)
                make.right.equalTo(self).offset(-25)
                make.bottom.equalTo(self).offset(-5)
            }
            
        } else if reuseIdentifier == "advisorReuse"{
            self.addSubview(advisorImage)
            self.addSubview(advisorView)
          //  self.addSubview(advisorSeriousLevel)
            self.advisorView.addSubview(advisorResponse)
            self.addSubview(advisorName)
            self.addSubview(advisorState)
            self.addSubview(advisorExperience)
            advisorImage.snp.makeConstraints { (make) -> Void in
                make.height.width.equalTo(30)
                // make.top.equalTo(self).offset(20)
                make.left.equalTo(self).offset(10)
                make.bottom.equalTo(self)
            }
            
            advisorView.snp.makeConstraints { (make) -> Void in
                //make.top.equalTo(self).offset(5)
                make.left.equalTo(advisorImage.snp.right).offset(5)
                make.right.equalTo(self).offset(-50)
                make.bottom.equalTo(advisorImage.snp.top).offset(30)
            }
            advisorResponse.snp.makeConstraints { (make) -> Void in
                make.top.equalTo(advisorView.snp.top).offset(10)
                make.left.equalTo(advisorView.snp.left).offset(10)
                make.right.equalTo(advisorView.snp.right).offset(-10)
                make.bottom.equalTo(advisorView.snp.bottom).offset(-10)
            }
            
            advisorName.snp.makeConstraints { (make) -> Void in
                make.top.equalTo(self).offset(5)
                make.left.equalTo(advisorView.snp.left).offset(10)
                //make.right.equalTo(self).offset(-50)
                make.bottom.equalTo(advisorView.snp.top).offset(-5)
            }
            
            advisorState.snp.makeConstraints { (make) -> Void in
                make.bottom.equalTo(advisorView.snp.top).offset(-5)
                make.top.equalTo(advisorName.snp.top)
                make.left.equalTo(advisorName.snp.right).offset(5)
                //make.right.equalTo(self).offset(-50)
            }
            
            advisorExperience.snp.makeConstraints { (make) -> Void in
                make.bottom.equalTo(advisorView.snp.top).offset(-5)
                make.top.equalTo(advisorState.snp.top)
                make.left.equalTo(advisorState.snp.right).offset(5)
               // make.right.equalTo(self).offset(-10)
             }
            
        } else if reuseIdentifier == "seriousReuse"{
            self.addSubview(advisorImage)
            self.addSubview(advisorName)
            self.addSubview(advisorState)
            self.addSubview(advisorExperience)
            self.addSubview(advisorSeriousLevel)
            
            advisorImage.snp.makeConstraints { (make) -> Void in
                make.height.width.equalTo(30)
                make.top.equalTo(self).offset(5)
                make.left.equalTo(self).offset(10)
            }
            
            advisorName.snp.makeConstraints { (make) -> Void in
                make.top.equalTo(advisorImage)
                make.left.equalTo(advisorImage.snp.right).offset(5)
                make.right.equalTo(self).offset(-50)
            }
            
            advisorState.snp.makeConstraints { (make) -> Void in
                make.top.equalTo(advisorName.snp.bottom).offset(5)
                make.left.equalTo(advisorImage.snp.right).offset(5)
                make.right.equalTo(self).offset(-50)
            }
            
            advisorExperience.snp.makeConstraints { (make) -> Void in
                make.top.equalTo(advisorState.snp.bottom).offset(5)
                make.left.equalTo(advisorImage.snp.right).offset(5)
                make.right.equalTo(self).offset(-50)
            }
            
            advisorSeriousLevel.snp.makeConstraints { (make) -> Void in
                make.top.equalTo(advisorExperience.snp.bottom).offset(5)
                make.left.equalTo(advisorImage.snp.right).offset(5)
                make.right.equalTo(self).offset(-50)
                make.bottom.equalTo(self).offset(-5)
            }
        }
    }
    
    // We won’t use this but it’s required for the class to compile
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    //Sickcall Bot
    lazy var botImage: UIImageView = {
        let image = UIImageView()
        image.backgroundColor = UIColor.black
        image.contentMode = .scaleAspectFill
        image.layer.cornerRadius = 15
        image.clipsToBounds = true
        image.image = UIImage(named: "angelina")
        return image
    }()
    
    lazy var botView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 5
        view.clipsToBounds = true
        view.backgroundColor = pastelGreen
        return view
    }()
    
    lazy var botQuestion: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "HelveticaNeue", size: 17)
        label.text = "Question"
        label.textColor = UIColor.black
        label.numberOfLines = 0
        return label
    }()
    
    lazy var botButton: UIButton = {
        let button = UIButton()
        button.setTitle("button", for: .normal)
        button.titleLabel?.font = UIFont(name: "HelveticaNeue-Bold", size: 20)
        button.setTitleColor(.black, for: .normal)
        button.layer.cornerRadius = 5
        button.backgroundColor = gray
        button.clipsToBounds = true
        return button
    }()
    
    //patient
    
    lazy var patientResponse: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "HelveticaNeue", size: 17)
        label.text = "Question"
        label.textAlignment = .right
        label.textColor = UIColor.black
        label.numberOfLines = 0
        return label
    }()
    
    lazy var patientView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 1
        view.clipsToBounds = true
        view.backgroundColor = tan
        //view.backgroundColor = UIColor(red: 223/255, green: 225/255, blue: 232/255, alpha: 1.0)
        return view
    }()
    
    lazy var patientHealthImage: UIImageView = {
        let image = UIImageView()
        image.backgroundColor = UIColor.black
        image.contentMode = .scaleAspectFill
        image.layer.cornerRadius = 3
        image.clipsToBounds = true
        image.image = UIImage(named: "appy")
        return image
    }()
    
    //nurse advisor
    lazy var advisorImage: UIImageView = {
        let image = UIImageView()
        image.backgroundColor = UIColor.black
        image.contentMode = .scaleAspectFill
        image.layer.cornerRadius = 15
        image.clipsToBounds = true
        image.image = UIImage(named: "angelina")
        return image
    }()
    
    lazy var advisorView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 5
        view.clipsToBounds = true
        view.backgroundColor = pastelBlue
        return view
    }()
    
    lazy var advisorName: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "HelveticaNeue", size: 14)
        label.text = "Test, RN"
        label.textColor = UIColor.black
        label.numberOfLines = 0
        return label
    }()
    
    lazy var advisorState: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "HelveticaNeue", size: 12)
        label.text = "California"
        label.textColor = UIColor.black
        label.numberOfLines = 0
        return label
    }()
    
    lazy var advisorExperience: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "HelveticaNeue", size: 12)
        label.text = "9 yrs experience"
        label.textColor = UIColor.black
        label.numberOfLines = 0
        return label
    }()
    
    lazy var advisorSeriousLevel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "HelveticaNeue", size: 17)
        label.text = "High"
        label.textColor = UIColor.black
        label.backgroundColor = UIColor.red
        label.numberOfLines = 0
        return label
    }()
    
    lazy var advisorResponse: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "HelveticaNeue", size: 17)
        label.text = "If the pain is primarily after eating fatty foods this could be a gallbladder issue. Visit your primary care doctor for official diagnosis and testing. If you have continued sharp pain visit an urgent care or ER."
        label.textColor = UIColor.black
        label.numberOfLines = 0
        return label
    }()
    
    //activity
    lazy var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.activityIndicatorViewStyle = .gray
        
        return activityIndicator
    }()
}
