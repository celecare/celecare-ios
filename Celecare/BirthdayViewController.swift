//
//  BirthdayViewController.swift
//  Celecare
//
//  Created by Dominic Smith on 12/15/17.
//  Copyright © 2017 Sickcall LLC. All rights reserved.
//

import UIKit
import SCLAlertView
import NVActivityIndicatorView
import SnapKit

class BirthdayViewController: UIViewController {
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "HelveticaNeue-Bold", size: 30)
        label.text = "What's your Birthday?"
        label.textColor = UIColor.black
        label.numberOfLines = 0
        return label
    }()
    
    lazy var datePicker: UIDatePicker = {
        let datePicker = UIDatePicker()
        datePicker.backgroundColor = .clear
        datePicker.datePickerMode = UIDatePickerMode.date
        datePicker.addTarget(self, action: #selector(datePickerChanged(sender:)), for: .valueChanged)
        return datePicker
        
    }()
    
    lazy var signupButton: UIButton = {
        let button = UIButton()
        button.setTitle("Done", for: .normal)
        button.titleLabel?.font = UIFont(name: "HelveticaNeue", size: 20)
        button.setTitleColor(.white, for: .normal)
        button.setBackgroundColor(uicolorFromHex(0x006a52), forState: .normal)
        button.titleLabel?.textAlignment = .center
        button.addTarget(self, action: #selector(self.buttonAction(sender:)), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(titleLabel)
        self.view.addSubview(datePicker)
        self.view.addSubview(signupButton)
        
        titleLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.view).offset(75)
            make.left.equalTo(self.view).offset(10)
            make.right.equalTo(self.view).offset(-10)
        }
        
        datePicker.snp.makeConstraints { (make) -> Void in
           // make.top.equalTo(titleLabel.snp.bottom).offset(75)
            make.bottom.equalTo(self.signupButton.snp.top)
            make.left.equalTo(self.view)
            make.right.equalTo(self.view)
        }
        
        signupButton.snp.makeConstraints { (make) -> Void in
            make.height.equalTo(50)
           // make.top.equalTo(datePicker.snp.bottom)
            make.bottom.equalTo(self.view)
            make.left.equalTo(self.view)
            make.right.equalTo(self.view)
        }
    }
    
    @objc func buttonAction(sender: UIButton){
        
    }
    
    @objc func datePickerChanged(sender: UIDatePicker) {
        
        print(sender.date)
    }
    
    
    func uicolorFromHex(_ rgbValue:UInt32)->UIColor{
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        
        return UIColor(red:red, green:green, blue:blue, alpha:1.0)
    }
}
